# Visma Digital Commerce: Improvements

This module is responsible for saving improvements and refactorings done to the Magento framework or other core modules.

![Example image](doc/images/exampel.png)

## Functionality

## Technical info

PHP documentation is available [here](doc/phpdoc/index.html).

## License

This module is proprietary software belonging to Visma Digital Commerce AS.
