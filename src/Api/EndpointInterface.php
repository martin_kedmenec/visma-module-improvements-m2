<?php

declare(strict_types=1);

namespace Visma\Improvements\Api;

/**
 * @api
 */
interface EndpointInterface
{
    /**
     * @param object $data
     * @return object
     */
    public function process(object $data): object;
}
