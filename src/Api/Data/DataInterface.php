<?php

declare(strict_types=1);

namespace Visma\Improvements\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

interface DataInterface extends ExtensibleDataInterface
{
    public const SOME_DATA = 'some_data';

    /**
     * @return mixed
     */
    public function getSomeData();

    /**
     * @param mixed $data
     * @return DataInterface
     */
    public function setSomeData($data): DataInterface;
}
