<?php

declare(strict_types=1);

namespace Visma\Improvements\Reflection;

class TypeProcessor extends \Magento\Framework\Reflection\TypeProcessor
{
    /**
     * @param $type
     * @return bool
     */
    public function isArrayType($type): bool
    {
        return (bool)preg_match('/(\[\]$|^ArrayOf|^array)/', $type);
    }
}
