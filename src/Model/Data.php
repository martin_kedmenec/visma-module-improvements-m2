<?php

namespace Visma\Improvements\Model;

use Magento\Framework\DataObject;
use Visma\Improvements\Api\Data\DataInterface;

class Data extends DataObject implements DataInterface
{
    public function getSomeData()
    {
        return $this->getData(self::SOME_DATA);
    }

    public function setSomeData($data): DataInterface
    {
        return $this->setData(self::SOME_DATA, $data);
    }
}
