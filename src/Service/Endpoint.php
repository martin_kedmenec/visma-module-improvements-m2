<?php

declare(strict_types=1);

namespace Visma\Improvements\Service;

use Visma\Improvements\Api\EndpointInterface;

class Endpoint implements EndpointInterface
{
    /**
     * @inheirtDoc
     */
    public function process(object $data): object
    {
        return $data;
    }
}
